-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 24 Février 2016 à 21:15
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `todoapp`
--

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_11_21_144703_create_todos_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `todos`
--

CREATE TABLE IF NOT EXISTS `todos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `completion_time` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=55 ;

--
-- Contenu de la table `todos`
--

INSERT INTO `todos` (`id`, `title`, `body`, `is_completed`, `completion_time`, `created_at`, `updated_at`) VALUES
(5, 'Ut asperiores unde est repellat.', 'Atque ratione rem similique maxime expedita nihil fuga. Et voluptatem ut consequuntur. Vitae incidunt consequatur saepe voluptas perferendis accusantium quas. Quisquam voluptatem aliquam amet cupiditate voluptatem magnam. Et sunt qui voluptate a repellendus.', 1, '2015-11-22 01:56:13', '2015-11-21 19:46:50', '2015-11-22 00:56:13'),
(6, 'Vero consequatur dolorem rerum quia.', 'Corporis vero quia est occaecati. Quod amet reiciendis fugit laudantium ipsam amet. Neque dolorem non illum inventore aut est explicabo doloribus.', 0, '2013-12-09 19:32:36', '2015-11-21 19:46:50', '2015-11-21 19:46:50'),
(7, 'Et sint vel illum quos sed.', 'Enim voluptatem est aut maiores dolorem tempore doloremque. Quasi enim rerum recusandae sed amet aspernatur veritatis. Et at quo voluptatem.', 1, '2008-06-02 03:18:04', '2015-11-21 19:46:50', '2015-11-21 19:46:50'),
(8, 'Commodi et dolor mollitia id sequi fugiat omnis.', 'Error delectus est temporibus sunt quis praesentium tempora expedita. Nihil est delectus tempora omnis mollitia quasi consectetur. Sit libero mollitia blanditiis praesentium incidunt sint.', 1, '1991-01-01 18:13:40', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(9, 'Suscipit id placeat cupiditate ut ea.', 'Sint esse porro dolorum ut necessitatibus inventore. Eaque ullam occaecati distinctio quibusdam dolorem. Dolor provident recusandae laboriosam ea in iste odio.', 1, '1980-11-07 03:40:10', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(10, 'Odit recusandae optio occaecati illo eos.', 'Et enim quam cum et beatae qui rerum voluptate. Accusantium in provident laboriosam non et. Possimus quos sit dolores aliquid.', 0, '2013-06-10 05:06:54', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(11, 'Deleniti quam amet perferendis sunt et ut.', 'Fugit facilis fugiat tempore sit facere. Odit quod eum dolore. Quisquam saepe rem dolor consequuntur.', 1, '2014-10-13 04:25:47', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(12, 'Aut voluptas aut et rerum pariatur labore ab.', 'Velit eos voluptatem cumque illum vitae ad aut. Voluptatum sed expedita et. Voluptatem cum illo ut et. Officiis doloribus alias modi rerum.', 0, '1987-01-04 16:34:06', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(13, 'Et eveniet dolores culpa sint qui quaerat.', 'Nam ipsum dicta possimus eligendi. Et dolores aut enim aliquam tenetur aut est. Suscipit accusantium quis rerum expedita ducimus neque non.', 0, '2005-04-17 16:40:40', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(14, 'Fuga ab consequatur blanditiis temporibus.', 'Sed sapiente et dolores eaque doloremque. Culpa cumque deserunt qui vitae omnis. Ipsa sed reprehenderit pariatur sint voluptas deserunt sint.', 1, '1982-03-18 16:27:22', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(15, 'Quae alias ut nihil repudiandae quo fugit quisquam.', 'Ratione totam distinctio et saepe sunt consequuntur. Totam eveniet laboriosam omnis facere dolorum. Quas in doloremque et non et placeat. Molestiae sit recusandae expedita.', 1, '2009-05-09 09:58:44', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(16, 'Et eum occaecati consectetur natus odit voluptas error asperiores.', 'Error nihil explicabo laborum enim veniam quibusdam magni. Non vitae qui eum modi ad sed. Sed officiis culpa est ut.', 0, '1973-11-08 19:22:46', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(17, 'Minus sequi iusto ea fugit.', 'Voluptate atque inventore voluptate et aut rem. Sunt quia aperiam neque ipsum qui et. Accusantium voluptatem debitis mollitia dolor. Et beatae molestiae doloremque magni minus facilis necessitatibus dolorum.', 0, '2005-06-18 17:28:09', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(18, 'Ut praesentium architecto molestiae suscipit magnam.', 'Sed est iste aliquid in qui provident optio. Ea praesentium ad tenetur quia voluptatum sit facere. Sed iusto non consequatur delectus sit harum.', 0, '2005-12-10 08:39:12', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(19, 'Reprehenderit eveniet repellendus consequuntur error.', 'In qui quasi debitis minima commodi reprehenderit ipsam. Saepe voluptatem tempora nisi voluptatibus molestias. Dolorem aspernatur rerum dolor qui velit molestiae.', 1, '1981-12-26 03:25:09', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(20, 'Ducimus ullam incidunt aut.', 'In quia qui quisquam voluptas omnis quia amet. Aliquid recusandae tempore asperiores facere aperiam facere et. Minima velit ipsa non perferendis possimus ut eos. Ratione rerum autem sequi quas et.', 1, '1984-08-07 20:31:24', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(21, 'Ut accusantium in dolor et.', 'Ut et temporibus deserunt debitis. Minus assumenda veritatis consectetur est numquam dolorem amet veritatis. Quidem voluptas alias accusamus aspernatur omnis dolorem. Voluptatem quo quibusdam eum.', 0, '1973-03-10 19:44:12', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(22, 'Architecto autem quo totam minus debitis sunt.', 'Quia autem repellendus odit neque fugiat molestias. Aut autem repellendus et quidem non. Et atque consequatur repudiandae nam accusantium esse. Et ab quia amet alias cum.', 0, '1986-06-27 20:21:45', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(23, 'Et corrupti occaecati itaque ipsa qui magnam.', 'Ipsum molestiae exercitationem eaque voluptatibus nemo. Itaque est animi ut eos et aliquam explicabo. Ipsam minus est aut eius velit. Inventore eaque iste quia sed et.', 0, '2014-06-07 07:30:34', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(24, 'Dolores architecto odit et enim.', 'Quaerat sint qui ipsa ad autem est. Est ut facere omnis quia praesentium. Iure similique modi quisquam. Perspiciatis non sunt tempore libero.', 1, '1974-05-16 18:46:03', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(25, 'Saepe libero placeat optio quae.', 'Inventore quasi animi sunt consequatur dolor consequatur. Assumenda aperiam error veritatis dolorum est in doloribus. Commodi sint et alias in.', 0, '1982-10-30 15:08:34', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(26, 'Aliquam expedita iste nulla eum nisi voluptatem.', 'Qui recusandae ratione cum veritatis dolor voluptatem deleniti. Fugit laudantium quam molestiae velit. Odit molestias maiores harum cum.', 0, '1987-06-04 15:10:43', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(27, 'Corrupti qui blanditiis perspiciatis omnis.', 'Ullam deleniti temporibus consequuntur consequatur. Velit reiciendis sunt sint nihil voluptates. Eum sed maiores et quis harum et error. Eum soluta corporis pariatur impedit.', 1, '2010-07-31 21:31:19', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(28, 'In voluptatem voluptas ad culpa.', 'Rerum et eum eius labore voluptatem blanditiis voluptatem. Possimus laborum dolores esse quod blanditiis doloribus. Ut totam qui velit laboriosam.', 0, '1977-07-04 01:46:18', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(29, 'Doloribus excepturi aut harum sapiente et quaerat.', 'Aliquam mollitia explicabo odio aut numquam rerum. Dolorem ut laudantium cumque possimus. Molestiae accusamus deserunt animi debitis ipsam.', 0, '2009-09-25 14:24:28', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(30, 'Similique et fugit iure pariatur dignissimos quasi voluptas cum.', 'Laborum consequatur dolore omnis consectetur et commodi vero. Fuga alias ut veniam molestiae quis qui aut. Rem sit quo doloribus nemo vel. Perspiciatis provident ducimus qui sint non laborum. Quia doloribus animi qui soluta et debitis qui placeat.', 1, '2003-05-08 03:46:01', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(31, 'Doloribus qui sit distinctio quia laudantium.', 'Esse possimus et nostrum alias. Optio exercitationem delectus maiores reprehenderit et sapiente rerum. Distinctio hic aut tempora. Ut reprehenderit vero pariatur qui placeat molestias.', 1, '1995-12-07 02:48:36', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(32, 'Repellat omnis similique dolore reiciendis cupiditate deserunt sequi.', 'Id sunt ad et hic et commodi. Quasi sed amet quas. Et nisi qui aliquid incidunt non.', 1, '2013-12-09 12:59:49', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(33, 'Labore corporis sit nesciunt ratione.', 'Molestias qui odio fugit quia non qui. Atque quasi ullam quo et.', 1, '2004-02-21 12:33:56', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(34, 'Aliquid officia incidunt cupiditate a provident officiis repellat non.', 'Veritatis ipsa corporis quis id atque. Maxime et corrupti molestias provident omnis reiciendis. Ratione quas iure quos odit. Corporis rerum et odio illum facere dolorem.', 1, '1986-01-04 00:05:48', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(35, 'Porro beatae sed quae beatae vel repellendus unde.', 'Eveniet necessitatibus omnis quasi eos et. Qui praesentium error autem. Magnam et consequatur rem sit. Pariatur voluptatem soluta sit saepe quo.', 1, '1971-05-09 10:38:05', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(36, 'Dolore ex explicabo molestias quisquam atque molestiae porro.', 'Sed asperiores assumenda ut et. Quaerat dolorem quidem autem ad. Iusto consequatur voluptatem ea sit autem. Repudiandae sit natus repellendus modi magnam architecto.', 0, '1987-01-21 07:53:39', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(37, 'Similique et optio voluptates doloribus quia reprehenderit et.', 'Et at temporibus explicabo eum recusandae. Sit hic ipsa aut molestiae. Voluptas voluptatem repellat autem officia culpa reprehenderit. Quisquam accusantium ducimus beatae sapiente enim assumenda.', 1, '2010-11-13 11:42:51', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(38, 'Architecto et magnam cumque sequi soluta.', 'Consequatur nostrum voluptas voluptatem qui magnam et. Quis corporis voluptate quia sapiente distinctio sit.', 0, '1989-08-07 21:37:04', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(39, 'Deleniti ut quidem velit odio molestiae qui.', 'Atque enim est id quia. Distinctio tempore repellat placeat omnis hic. Omnis et qui perspiciatis consequatur non. Ipsam occaecati error ut et et.', 1, '2006-10-04 21:59:22', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(40, 'Non perferendis qui iste voluptatem voluptate doloremque debitis beatae.', 'Et consequatur molestiae exercitationem suscipit fugit quae. Qui nemo architecto architecto laudantium est error. Necessitatibus nostrum quasi ut. Natus sit minus nesciunt.', 1, '2006-01-03 11:05:35', '2015-11-21 19:46:51', '2015-11-21 19:46:51'),
(41, 'Nihil provident ab et voluptas consequuntur distinctio nemo.', 'Qui molestias incidunt aut et odio omnis. Sed sint velit laudantium fugiat. Quae sed ad ullam minus magnam. Pariatur earum molestiae voluptas perferendis nihil quidem.', 0, '1976-09-14 12:14:17', '2015-11-21 19:46:52', '2015-11-21 19:46:52'),
(42, 'Aliquam quo et dolore quibusdam ut.', 'Nemo et qui placeat. Ducimus dolores earum quaerat minima consequatur aspernatur debitis praesentium. Quaerat iste quasi velit voluptate libero voluptatem. Odit odio aut et corrupti dolores.', 1, '1975-01-17 13:50:46', '2015-11-21 19:46:52', '2015-11-21 19:46:52'),
(43, 'Dolor sed ab facere exercitationem laborum quia quod quam.', 'Praesentium doloribus eveniet quas autem numquam consequatur quidem. Veniam perferendis possimus facilis. Voluptatem nam delectus cum porro nihil voluptates ducimus. In laborum ratione et libero dolores. Voluptatum et libero voluptates ex et et id.', 1, '1993-12-26 06:23:21', '2015-11-21 19:46:52', '2015-11-21 19:46:52'),
(44, 'Quam alias unde est iusto dolorem.', 'Nihil numquam accusantium et vero asperiores perspiciatis in. Et eum impedit commodi quibusdam voluptatem officiis molestias. Nemo accusamus quidem assumenda culpa rerum et quam. Est quibusdam voluptate quasi repellat reprehenderit deserunt nostrum consectetur.', 1, '1991-09-25 02:44:45', '2015-11-21 19:46:52', '2015-11-21 19:46:52'),
(45, 'Dolor modi et consequatur accusantium ab libero nobis.', 'Aliquam perspiciatis nemo doloremque temporibus. Et architecto reiciendis qui. Illum reiciendis nostrum ipsum explicabo voluptate autem aut.', 1, '2015-11-22 01:34:57', '2015-11-21 19:46:52', '2015-11-22 00:34:57'),
(46, 'Provident aliquam nulla qui itaque eaque qui assumenda.', 'Nam neque voluptas qui aut aspernatur blanditiis. Neque iste omnis ea corrupti pariatur. Quod ad suscipit quo ut ipsa blanditiis.', 0, '2009-09-25 03:22:48', '2015-11-21 19:46:52', '2015-11-21 19:46:52'),
(47, 'Facilis omnis eum odit quae ut blanditiis nam.', 'Quia voluptatem omnis deleniti quos sapiente et et. Rem voluptas nihil delectus rerum itaque mollitia minus. Consequatur est consequuntur eum sit natus.', 0, '1985-11-14 07:54:18', '2015-11-21 19:46:52', '2015-11-21 19:46:52'),
(48, 'Est rerum harum in hic quo dolor.', 'Nihil non culpa error qui. Repellat eum pariatur quo consequatur. Officia qui id vel et aut. Eos aut repellendus placeat rem architecto incidunt officia.', 1, '2006-06-24 09:11:28', '2015-11-21 19:46:52', '2015-11-21 19:46:52'),
(49, 'Quia optio totam dolores nihil.', 'Voluptatibus placeat est iusto omnis. Est consequatur eum sunt cum odit labore ut. Qui est id iure eligendi.', 1, '1972-08-16 21:19:21', '2015-11-21 19:46:52', '2015-11-21 19:46:52'),
(50, 'Nulla eos eum veniam.', 'Cum vel eligendi quasi ipsam. Sit sint maxime in enim distinctio. Ut cumque quod voluptatum quo quia sed minima voluptatum.', 0, '2013-08-08 13:24:01', '2015-11-21 19:46:52', '2015-11-21 19:46:52'),
(51, 'my  first todo ', 'this is the todo  body', 1, '2015-11-22 00:59:40', '2015-11-21 23:28:56', '2015-11-21 23:59:40'),
(53, 'lkqsklqs 636633', 'sdcklqsdklqkld\r\nsqsdkqkjsd\r\n\r\nsdqsdkqsd', 1, '2015-11-22 00:58:00', '2015-11-21 23:30:19', '2015-11-21 23:58:00'),
(54, 'common the fuckers ', 'how is that cool', 1, '2015-11-22 01:01:00', '2015-11-21 23:49:12', '2015-11-22 00:01:00');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

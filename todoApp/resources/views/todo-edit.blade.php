@extends('layout')


@section('content')

<h2> Edit Todo : </h2>
 
{!!
 Form::model($todo, array('route' => array('todoapp.update', $todo->id), 'method' => 'PUT')) !!}

<div class="form-group">
	{!! Form::text('title' ,null , ['class' => 'form-control'])  !!}
</div>

<div class="form-group">

	{!! Form::textarea('body', null ,  ['class' => 'form-control'])  !!}
</div>
<div class="form-group">
	<span>completed </span>
	{!! Form::checkbox('is_completed', '1' )  !!}
</div>

<div class="form-group">

	{!! Form::submit('Save',  ['class' => 'btn  btn-primary']) !!}
</div>
{!!Form::close()!!}
@stop  
@extends('layout')

@section('content')

<h2> Todo Details </h2>

  		<ul class="list-group">
		  		<li class="list-group-item">
		  			<h3>
		  				{{$todo->title}} 
		  			</h3>
		  			<hr>
		  			<div>
		  				{{$todo->body}}
		  			</div>
		  			<hr>
		  			<i>
			 			@if($todo->is_completed) 
			 			<span class="label label-success">Completed</span>
			 			@else 
			 				<span class="label label-danger">Not Completed</span>
			 			@endif
		  			</i> 
		  			@if($todo->is_completed) 
			  			<code>
			  				completion time  : {{$todo->completion_time}}
			  			</code>
		  			@endif

		  			<a href= "{{$todo->id}}/edit">
		  				<span class="glyphicon glyphicon-pencil"></span>
		  			</a>	
		  			@if(!$todo->is_completed) 
			  		<form action="complete/{{$todo->id}}" method="POST" class="clearForm">
	   				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	   				    <button class="clearForm" type="submit"  > <span class="glyphicon glyphicon-ok"></span> </button>
					</form>
					@endif

		  			<form action="{{$todo->id}}" method="POST" class="clearForm">
	   				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	   				        <input type="hidden" name="_method" value="DELETE">
	   				    <button class="clearForm" type="submit" > 
	   				    	<span class="glyphicon glyphicon-remove"></span> 
	   				    </button>
					</form>

		  		</li>
	  	</ul>
@stop  
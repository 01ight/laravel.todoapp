<!DOCTYPE html>
<html>
<head>
	<title> todoApp</title>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<style>
	.clearForm{
		display: inline;
    	border: none;
    	background: none;"
	}
</style>
<body>
 

	  <div class="container">
	  	<div class= "row">
	  		<div class="col-md-8 col-md-offset-2">
		  		@yield('content') 
		  	</div>
		</div>
	  </div>

</body>

</html>
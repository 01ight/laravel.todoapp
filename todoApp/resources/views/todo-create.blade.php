@extends('layout')

@section('content')
	<h3> Create Todo</h3>
	{!! Form::open( array('route' => array('todoapp.store'), 'method' => 'POST')) !!}

<div class="form-group">

	{!! Form::text('title' ,null , ['class' => 'form-control', 'placeholder' => 'title'])  !!}
</div>

<div class="form-group">

	{!! Form::textarea('body', null ,  ['class' => 'form-control' ,'placeholder' => 'body ...'])  !!}
</div>

<div class="form-group">
<span > completed </span>
	{!! Form::checkbox('is_completed', '1')  !!}
</div>

<div class="form-group">
	{!! Form::submit('create',  ['class' => 'btn  btn-primary']) !!}
</div>
{!!Form::close()!!}

{!! link_to_route('todoapp.index', 'back', '', array('class'=> 'btn btn-success')) !!}
@stop  
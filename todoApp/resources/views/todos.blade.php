@extends('layout') 

@section('content') 	  	
  		<h1> All  todos</h1> 
  		<a href="todoapp/create" class= "btn btn-danger"> New </button></a>
  		<ul class="list-group">
		  	@foreach ($todos  as  $todo )
		  		<li class="list-group-item">
		  			<h3>
		  				<a href="todoapp/{{$todo->id}}">	{{$todo->title}} </a>
		  			</h3>
		  			<hr>
		  			<div>
		  				{{str_limit($todo->body, 90) }}
		  			</div>
		  			<hr>
		  			<i> 
			 			@if($todo->is_completed) 
			 			<span class="label label-success">Completed</span>
			 			@else 
			 				<span class="label label-danger">Not Completed</span>
			 			@endif
		  			</i>

		  			@if($todo->is_completed) 
			  			<code>
			  				completion time  : {{$todo->completion_time}}
			  			</code>
		  			@endif
		  			<a href= "todoapp/{{$todo->id}}/edit">
		  				<span class="glyphicon glyphicon-pencil"></span>
		  			</a>
					
					@if(!$todo->is_completed) 

			  		<form action="todoapp/complete/{{$todo->id}}" method="POST" class="clearForm">
	   				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	   				    <button class="clearForm" type="submit" class="clear"> <span class="glyphicon glyphicon-ok"></span> </button>
					</form>
					
					@endif
		  		
		  			<form action="todoapp/{{$todo->id}}" method="POST" class="clearForm">
	   				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	   				        <input type="hidden" name="_method" value="DELETE">
	   				    <button class="clearForm" type="submit" class="clear"> 
	   				    	<span class="glyphicon glyphicon-remove"></span> 
	   				    </button>
					</form>

		  		</li>
		  		<br>
		  	@endforeach  
	  	</ul>
	  	{!!$todos->render()!!}
@stop

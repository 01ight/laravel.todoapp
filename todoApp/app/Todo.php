<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
   
protected   $fillable  = [
		 			'title', 
		 			'body', 
		 			'is_completed', 
		 			'completion_time'
		 	] ; 

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Todo ; 
class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todo::paginate(10)   ;
        $todos->setPath('todoapp') ; 
        return view('todos', compact('todos')) ; 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todo-create') ; 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $todo = new Todo($request->all()) ;
         // dd($request->all() ) ;
         // $todo->title =  $request->all()['title'] ; 
         // $todo->body = $request->all()['body']; 
         // $todo->is_completed =  ['is_completed']; 

         $todo->save() ;
         return  \Redirect::action('TodosController@index') ; 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $todo = Todo::findOrFail($id) ;

        return view('todo-show',  compact('todo') ) ; 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $todo = Todo::findOrFail($id) ;

        return view('todo-edit', compact('todo') ); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $todo =  Todo::where('id' ,$id)->first() ; 
        $todo->title =  $request->all()['title'] ; 
        $todo->body =  $request->all()['body'] ;
        $todo->is_completed=  isset($request->all()['is_completed'] ) ? 1 : 0 ; 

        if(isset($request->all()['is_completed']))  {

            $todo->completion_time =  \Carbon\Carbon::now()->toDateTimeString() ;
        }
        $todo->save() ; 

        return \Redirect::action('TodosController@index') ;  

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Todo::destroy($id) ;
         return  \Redirect::action('TodosController@index') ; 

    }

    /**
     * complete a  todo 
     *
     * @param  int  $id
     * @param  Request $request 
     * @return \Illuminate\Http\Response
     */
    public function complete(Request $request , $id)
    {

        $todo= Todo::where('id',  $id)->first()  ;
        $todo->is_completed = 1 ; 
        $todo->completion_time =  \Carbon\Carbon::now()->toDateTimeString() ;
        $todo->save() ; 
        return \Redirect::action('TodosController@index') ; 

    } 
}
